FROM mcr.microsoft.com/dotnet/sdk:6.0-jammy-amd64 AS build
ARG TRANSFER_IMAGE_TAG

RUN git clone --quiet --branch ${TRANSFER_IMAGE_TAG} --depth 2 https://gitlab.com/sis-cc/.stat-suite/dotstatsuite-core-transfer.git /app

WORKDIR /app

RUN dotnet restore DotStatServices.Transfer 
RUN dotnet publish DotStatServices.Transfer -c Release --no-restore -o /out

FROM mcr.microsoft.com/dotnet/aspnet:6.0-jammy-amd64 AS runtime
WORKDIR /app

COPY --from=build /out .
COPY scripts/transfer-log-filter.sh /app/log-filter.sh

ENV DOTNET_SYSTEM_GLOBALIZATION_INVARIANT=false
ENV auth__scopes__0=openid
ENV auth__scopes__1=profile
ENV auth__scopes__2=email
ENV auth__claimsMapping__email=email
ENV auth__claimsMapping__groups=groups
ENV auth__requireHttps="false"
ENV auth__validateIssuer="false"
ENV MaxTransferErrorAmount=0
ENV MinPercentageDiskSpace=15
ENV DefaultLanguageCode=en
ENV MaxTextAttributeLength=150
ENV SpacesInternal__0__Id=dc-design
ENV SpacesInternal__0__DataImportTimeOutInMinutes=2
ENV SpacesInternal__0__DatabaseCommandTimeoutInSec=60
ENV SpacesInternal__0__AutoLog2DB="true"
ENV SpacesInternal__0__AutoLog2DBLogLevel="Notice"
ENV SpacesInternal__0__EmailLogLevel="Warn"
ENV SpacesInternal__1__Id=dc-release
ENV SpacesInternal__1__DataImportTimeOutInMinutes=2
ENV SpacesInternal__1__DatabaseCommandTimeoutInSec=60
ENV SpacesInternal__1__AutoLog2DB="true"
ENV SpacesInternal__1__AutoLog2DBLogLevel="Warn"
ENV SpacesInternal__1__EmailLogLevel="Warn"
ENV Kestrel__Limits__MaxRequestBodySize=300000000

COPY log4net.config /app/config/log4net.config

ENTRYPOINT ["./log-filter.sh"]

FROM runtime as testbetrieb

FROM runtime
